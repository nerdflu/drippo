Drippo
Sends orders from WooCommerce to Drip's Shopper Activity API. 
Copyright (C) 2019 [Mike Hancock](https://unfamo.us/drippo)

Send's WooCommerce orders to Drip via the Shopper activity API.

Notes:

1. Currenly, this only sends new orders and updates order status. 
2. If the order is on hold or pending payment it won't be sent until status has changed.
3. First version! More features coming :) Please [send any feedback here](https://unfamo.us/drippo)

Thanks!

                                                          ,▄▄█`
                                                     ,▄▓▓▓▓▀
                                                 ╓▄█▀▀░░▓▓
                                             ,▄▓▀▀░░░░░╣▓
                                          ,▄▓▀░░░░░░░░░▓▌
                                       ,▄▓▀░░░░░░░░░░░░╣▓
                                     ▄▓▓░╩╬░░░░░░░░░░░░░▓▌
                                   ▄▓▀░╜ ,╬░░░░░░░░░░░░░░▓▓
                                 ▄▓▀░░░═╦░░░░░░░░░░░░░░░░░▀▓▄
               ╓▄▄▄            ▄▓▓░░░░╬╬░░░░░░░░░░░░░░░░░░░░▀▓▄
             ╓▀    ╙▌         ▓▓░░░░░░░▄▓▓▓▓▓▌░░░╬▓▓▓▓▓░░░░░░░▓▓
             ▓     █¬       ,▓▓░░░░░░▓▓▀▀░░░╬░░░░░░░▀▀▓▓▓░░░░░░▀▓▄
             ▓   ╒▀        ╓▓▀░░░░░░╬░░▓▀▀▀▀▄░░░░░▓▀▀▀▓░▀▓░░░░░░░▓▓
     ▄▀▀~¬`¬▀▀▓▄ ▓        ╓▓▌░░░░░░░░░▓     ╙▌░░▓▀     ▓░░░░░░░░░░▓▓
     ▓         ^▌▐▄       ▓▓░░░░░░░░░╬▌      ▓░▓       ▓╬░░░░░░░░░░▓▓
     ▓▀╗▄,   ,▄▓` ▓      ▓▓╬░░░░░░░░░░▌  ╒▓▓µ╟▓  ▓▓    ▓╬░░░░░░░░░░░▓▓
     ▓    ¬,,  ▐▌╓▌     ]▓▌░░░░░░░░░░░▀µ ▐▓▓▌▓▓ ▓▓▓▌  ▓░░░░░░░░░░░░░░▓▌
    ]▌▀╗▄     ▄█▄█▌     ╟▓░░░░░░░░▓▓▀░░░▒▄▄Æ▀░▀⌐ ▀▀ ▄▓░░░░░░░░░░░░░░░▓▓
     ▓   └▀▀▀.▓▀¬╒▓     ▓▓╬░░░░░░░╬▀▓░░░░░░░░░░▀▒Æ▒▀░░░░▓▓░░░░░░░░░░╫▓▓
      ▀▀ªKK▓▓▌,▄▄▓▄     ▓▓░░░░░░░░░░░▀█▓▄░░░░░░░░░░░╬╬▓▓▓░▀░░░░░░░░╣╬▓▓
             ,,╙▓▓▓▓▓▓▓▓▓▓▌░░░░░░░░░░░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▀░░░░░░░░░╣▓▓▓▓▓▄
                  ▀▀███▀▀▓▓░░░░░░░░░░░░░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░░░░░░░░░░▒╬╬▓▓▓▓▓▓
                          ▓▓░░░░░░░░░░▓▓▓▀░░▓▀▀▀▀▀▓▓▓▀░░░░░░░░░╣▒╬╣▓▀  ▀▓▓▓▄
                           ▀▓▓░░░░░░░░░░▀▀█▓▄▄▄▓█▀▀░░░░░░░░░░▒▒╬╬▓▓▀    ▀▓▓▓µ
                            "▓▓▓╬░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▓╬╬▓▓▀       ▓▓▓▓,
                              ╙█▓▓╬▒╬╬░░░░░░░░░░░░░░░╬▒▒▓╬╬╬╬▓▓▀└       ▀▐▓▓▓⌐'▓
                                 ▀█▓▓▓╬╬▓▒▒▒▒▒▒▒▒▓▓╬╬╬╬╬╬▓▓▓█▀        ╓█▄ ▀▀▀ ,▓▀█
                                    └▀▀▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▀╙          ▓¬ '▀▀▀▀▀└   ▓
                                       ▓▓▓▓▀▀▀▀▀▀▀▀▓▓▓▌             ╟▌          ⁿµ╟▌
                                       ▓▓▓▓       j▓▓▓⌐             ▐▌ ▀█        ▐▓
                                       ▐▓▓▓       ▐▓▓▓                ▀▀▌└    ▓  ▓Ö
                              ,▄▄▓▀▓▓▀▓▓▓▓▓▌▄   ,▄▓▓▓▓▓▓▓▓▓█▄▄,         ╙█▄▄▄▀▀▀▀
                           ▄▀▀▀▀▀▓▓▒▄▓▓╬░▀░╬░▓ ╟▌╬░░░░▓▓▓Æ▓▓▓▀▀▀▀▄
                         ╓▌        ╙▀░╬╬╬╬╬▓▓"  ▀▓▒╬╬╬╬╬╬▀`       ▀▄
                         ▓▓          ▐█▀▓▓▀└     '▀▓▓▀╬▓         ╓╩▓
                          ▀█▄▄▄▄▄▄▄▄▄█▀▀             ▀▀█▄▄▄▄▄▄▄▄▄█"
                                                           
                                 - Drippys mate, Drippo.