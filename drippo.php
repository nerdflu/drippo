<?php
/**
 * Plugin Name: Drippo
 * Description: Drip Shopper Activity for WooCommerce.
 * Plugin URI: https://unfamo.us
 * Author: Mike Hancock
 * Version: 1.0.3
 * Author URI: https://unfamo.us
 * Requires at least:   4.9
 * Tested up to:        5.2.2
 *
 * Text Domain: drippo
 *
 * @package drippo
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/***
 * Set constants.
 */
define( 'DRIPPO_VERSION', '1.0.0' );

/* Debug output control. */
if ( ! defined( 'DRIPPO_DEBUG_OUTPUT' ) ) {
	define( 'DRIPPO_DEBUG_OUTPUT', false );
}
define( 'DRIPPO_SLUG', 'drippo' );
define( 'DRIPPO_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'DRIPPO_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

add_action( 'plugins_loaded', 'drippo_load_plugin_textdomain' );
if ( ! version_compare( PHP_VERSION, '5.4', '>=' ) ) {
	add_action( 'admin_notices', 'drippo_fail_php_version' );
} else {
	require_once DRIPPO_PATH . 'inc/bootstrap.php';
}

/**
 * Load gettext translate for our text domain.
 */
function drippo_load_plugin_textdomain() {
	load_plugin_textdomain( 'drippo' );
}

/**
 * Show in WP Dashboard notice about the plugin is not activated.
 */
if ( ! function_exists( 'drippo_fail_php_version' ) ) {
	function drippo_fail_php_version() {
		$message      = esc_html__( 'drippo requires PHP version 5.4+, plugin is currently NOT ACTIVE.', 'drippo' );
		$html_message = sprintf( '<div class="error">%s</div>', wpautop( $message ) );
		echo wp_kses_post( $html_message );
	}
}