<?php
/**
 * Drippo: Bootstrap File
 *
 * This starts things up. Registers the SPL and starts up some classes.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

spl_autoload_register(
	function ( $class ) {
		$prefix   = __NAMESPACE__;
		$base_dir = __DIR__;
		$len      = strlen( $prefix );
		if ( strncmp( $prefix, $class, $len ) !== 0 ) {
			return;
		}
		$relative_class = strtolower( substr( $class, $len + 1 ) );
		$relative_class = 'class-' . $relative_class;
		$file           = $base_dir . DIRECTORY_SEPARATOR . str_replace( [ '\\', '_' ], [ '/', '-' ], $relative_class ) . '.php';
		if ( file_exists( $file ) ) {
			require $file;
		} else {
			wp_die( esc_html( 'Filename: ' . basename( $file ) . ' missing.' ) );
		}
	}
);

Settings::get_instance();
Plugin::get_instance();
Abcart::get_instance();
Drip::get_instance();

if(is_admin()){
    Admin::get_instance();
}