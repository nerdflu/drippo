<?php
/**
 * Drippo: Abcart class
 *
 * This handles all our hooks and stuff.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

class Abcart extends Base {

    public $drip;

    public function __construct() {
        $this->drip = Drip::get_instance();

        add_action( 'woocommerce_add_to_cart_fragments', array( $this, 'cart_update' ));

    }

    public function cart_update($fragments){
        ob_start();
        ?>
        <script id="cart-update">console.log('hello <?php echo WC()->cart->get_cart_contents_count(); ?>');</script>
        <?php
        $fragments['#cart-update'] = ob_get_clean();
        return $fragments;
    }
}