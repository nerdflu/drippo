<?php
/**
 * Drippo: Admin Class
 *
 * This is where all of the adminy things are.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

class Admin extends Base {
    const SETTINGS_PAGE_SLUG = 'drippo';
    public function __construct() {
        add_action('admin_menu', array($this, 'add_admin_pages'));
    }
    
    /**
     * Create settings page menu
     **/ 
    public function add_admin_pages(){

        // add menu and page to wordpress sidebar

        $settings_page = add_options_page(
            'Drippo Settings',
            'Drippo',
            'manage_options',
            'Drippo',
            array($this,'settings_page')
        );
        add_action( 'load-' . $settings_page, array($this, 'enqueue' ) );
    }

    /**
     * Load settings page template
     **/ 
    public function settings_page(){
        require_once plugin_dir_path(__FILE__). 'settings.php';
    }

    public function enqueue(){

        // admin js & css. 
        wp_enqueue_style( 'drippo-style', plugins_url('../assets/css/drippo-admin.css' , __FILE__ ) );

        // parsley is a form validation framework
        wp_enqueue_script( 'drippo-script-parsley', plugins_url('../assets/js/parsley.min.js' , __FILE__ ) );
        
    }

    /**
     * Admin notices
     */
    public function wordpress_notice($type,$message){
        echo '<div class="'. $type .' notice is-dismissible"><p>'.$message.'</p></div>';
    }
}