<?php
/**
 * Drippo: Base class
 *
 * Provides some core helper methods for a base class that we can extend.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

class Base {

    const PAGE_SLUG = DRIPPO_SLUG;

    /**
	 * Holds the plugin instance.
	 */
    private static $instances = [];

    /**
	 * Sets up a single instance of the plugin.
	 */
	public static function get_instance() {
		$module = get_called_class();
		if ( ! isset( self::$instances[ $module ] ) ) {
			self::$instances[ $module ] = new $module();
		}
		return self::$instances[ $module ];
    }

    /**
	 * Sets up a url to the admin page.
	 */
    public function get_url() {
		return admin_url( 'admin.php?page=' . self::PAGE_SLUG );
	}
} 