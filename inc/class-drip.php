<?php
/**
 * Drippo: Drip class
 *
 * This is where all of the drip magic happens.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

class Drip extends Base {
    private $accountnumber;
    private $apikey;
    
    function __construct()
    {
        $Settings = Settings::get_instance();
        $this->accountnumber = $Settings->accountnumber;
        $this->apikey = $Settings->apikey;

        add_action( 'wp_footer', array($this,'drip_tracking_code'));
    }

    /**
     * Posting to Drip API V2. eg: subscribers/tags/events.
     */

    public function drip_post($endpoint,$data){
        $url = 'https://api.getdrip.com/v2/' . $this->accountnumber . '/' . $endpoint;
        $res = wp_remote_post( $url, array(
                'method'  => 'POST',
                'timeout' => 10,
                'headers' => array(
                    'Content-Type'  => 'application/json',
                    "User-Agent"    => "Drippo (unfamo.us)",
                    "Authorization" => 'Basic ' . base64_encode( $this->apikey . ':' ),
                ),
                'body'    => json_encode([
                    $endpoint => [
                        $data
                    ]
                ]),
            )
        );
        $response = $res['response']['message'];
        return $response;
    }

    /**
     * Posting to Drip API v3. eg: shopper_activity.
     */
    public function drip_postv3($endpoint,$data){
        $url = 'https://api.getdrip.com/v3/' . $this->accountnumber . '/' . $endpoint;
        $res = wp_remote_post( $url, array(
                'method'  => 'POST',
                'timeout' => 10,
                'headers' => array(
                    'Content-Type'  => 'application/json',
                    'User-Agent'    => 'Drippo (unfamo.us)',
                    'Authorization' => 'Basic ' . base64_encode( $this->apikey . ':' ),
                ),
                'body'    => json_encode($data)
            )
        );
        $response = $res['response']['message'];
        return $response;
    }

    /**
     * Identify email address, used on capture or order placed.
     */

    public function identify($email){
        echo "<script type='text/javascript'>
        if (typeof _dcq !== 'undefined') {
            _dcq.push(['identify',{email:'$email', success:function(res){console.log('$email')}}])
        }</script><!-- End Drippo -->";
        }

    public function drip_tracking_code(){
        echo "<!-- Drip -->
        <script type='text/javascript'>
        var _dcq = _dcq || [];
        var _dcs = _dcs || {};
        _dcs.account = '".$this->accountnumber."';

        (function() {
            var dc = document.createElement('script');
            dc.type = 'text/javascript'; dc.async = true;
            dc.src = '//tag.getdrip.com/".$this->accountnumber.".js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(dc, s);
        })();
        </script>
        <!-- end Drip -->";
    }
}