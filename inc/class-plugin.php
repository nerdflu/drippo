<?php
/**
 * Drippo: Plugin class
 *
 * This handles all our hooks and stuff.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

class Plugin extends Base {

    public $drip;

    public function __construct() {
        $this->drip = Drip::get_instance();

        // Identify user after purchase.
        add_action( 'woocommerce_thankyou', array($this,'identify_sub'));
        //add_action( 'woocommerce_thankyou', array($this,'shopper_activity_update_order'));

        // Order updates
        add_action( 'woocommerce_order_status_failed', array($this,'shopper_activity_update_order'));
        add_action( 'woocommerce_order_status_processing', array($this,'shopper_activity_update_order'));
        add_action( 'woocommerce_order_status_completed', array($this,'shopper_activity_update_order'));
        add_action( 'woocommerce_order_status_refunded', array($this,'shopper_activity_update_order'));
        add_action( 'woocommerce_order_status_cancelled', array($this,'shopper_activity_update_order'));
    }

    /**
     * Identify user on purchase.
     */
    public function identify_sub($order_id){
        $order = wc_get_order( $order_id );
        $order_data = $order->get_data();
        $email = $order_data['billing']['email'];
        $this->drip->identify($email);
    }
    
    /** 
     * Order update / New order
     */
    public function shopper_activity_update_order($order_id){
        $order = wc_get_order( $order_id );
        $order_data = $order->get_data();
    
        $email              = $order_data['billing']['email'];
        $orderurl           = admin_url( 'post.php?post=' . absint( $order->get_id() ));
        $ordertotal         = $order->get_total();
        $discounttotal      = $order->get_discount_total();
        $ordertaxes         = $order->get_total_tax();
        $totalshipping      = $order->get_shipping_total();

        // Get billing details from order
        $bill_first_name    = (empty($order_data['billing']['first_name'])) ? '' : $order_data['billing']['first_name'];
        $bill_last_name     = (empty($order_data['billing']['last_name'])) ? '' : $order_data['billing']['last_name'];
        $bill_company       = (empty($order_data['billing']['company'])) ? '' : $order_data['billing']['company'];
        $bill_address_1     = (empty($order_data['billing']['address_1'])) ? '' : $order_data['billing']['address_1'];
        $bill_address_2     = (empty($order_data['billing']['address_2'])) ? '' : $order_data['billing']['address_2'];
        $bill_city          = (empty($order_data['billing']['city'])) ? '' : $order_data['billing']['city'];
        $bill_state         = (empty($order_data['shipping']['state'])) ? '' : $order_data['billing']['state'];
        $bill_postcode      = (empty($order_data['billing']['postcode'])) ? '' : $order_data['billing']['postcode'];
        $bill_country       = (empty($order_data['billing']['country'])) ? '' : $order_data['billing']['country'];
        $bill_phone         = (empty($order_data['billing']['phone'])) ? '' : $order_data['billing']['phone'];

        // Get shipping details from order
        $ship_first_name    = (empty($order_data['shipping']['first_name'])) ? '' : $order_data['shipping']['first_name'];
        $ship_last_name     = (empty($order_data['shipping']['last_name'])) ? '' : $order_data['shipping']['last_name'];
        $ship_company       = (empty($order_data['shipping']['company'])) ? '' : $order_data['shipping']['company'];
        $ship_address_1     = (empty($order_data['shipping']['address_1'])) ? '' : $order_data['shipping']['address_1'];
        $ship_address_2     = (empty($order_data['shipping']['address_2'])) ? '' : $order_data['shipping']['address_2'];
        $ship_city          = (empty($order_data['shipping']['city'])) ? '' : $order_data['shipping']['city'];
        $ship_state         = (empty($order_data['shipping']['state'])) ? '' : $order_data['shipping']['state'];
        $ship_postcode      = (empty($order_data['shipping']['postcode'])) ? '' : $order_data['shipping']['postcode'];
        $ship_country       = (empty($order_data['shipping']['country'])) ? '' : $order_data['shipping']['country'];
        $ship_phone         = (empty($order_data['shipping']['phone'])) ? '' : $order_data['shipping']['phone'];
        
        // Arrays for each
        $billing = array(
            'first_name'       => $bill_first_name,
            'last_name'        => $bill_last_name,
            'company'          => $bill_company,
            'address_1'        => $bill_address_1,
            'address_2'        => $bill_address_2,
            'city'             => $bill_city,
            'state'            => $bill_state,
            'postal_code'      => $bill_postcode,
            'country'          => $bill_country,
            'phone'            => $bill_phone
        );
        
        $shipping = array(
            'first_name'       => $ship_first_name,
            'last_name'        => $ship_last_name,
            'company'          => $ship_company,
            'address_1'        => $ship_address_1,
            'address_2'        => $ship_address_2,
            'city'             => $ship_city,
            'state'            => $ship_state,
            'postal_code'      => $ship_postcode,
            'country'          => $ship_country,
            'phone'            => $ship_phone
        );

        // Items
        $items = $order->get_items();
        foreach ( $items as $item ) {
            
            $variant_price      = get_post_meta( $item['variation_id'],'_price',true);
            $product            = new \WC_Product( $item['product_id'] );

            if( $item->get_variation_id() > 0 ){ 
                $product = $item->get_product(); 
                $product_price      = (int) $product->get_price();
                $product_sku        = get_post_meta( $item['variation_id'], '_sku', true );
            } else {
                $product = $item->get_product(); 
                $product_price      = (int) $product->get_price();
                $product_sku        = $product->get_sku();
            }

            $item_list[] = array(
                'product_id'            => (string) $item['product_id'],
                'product_variant_id'    => (string) $item['variation_id'],
                'sku'                   => $product_sku,
                'name'                  => $item['name'],
                'quantity'              => $item['qty'],
                'price'                 => $product_price,
                //'total'               => floatval( $product_price * $item['qty'] ), -- It looks like drip is multiplying this number by qty. 
                'image_url'             => get_the_post_thumbnail_url($item['product_id'],'thumbnail'),
                'product_url'           => get_permalink( $item['product_id'] )
            );
        
            //echo $product_price;
        }

        // Order status
        $order_status = $order_data['status'];
        if ( $order_status == 'processing') 		{ $order_action = 'placed'; }
        elseif ($order_status == 'completed')		{ $order_action = 'fulfilled'; }
        elseif ($order_status == 'refunded')        { $order_action = 'refunded'; }
        elseif ($order_status == 'failed')          { $order_action = 'canceled'; }
        elseif ($order_status == 'on hold')         { $order_action = false; }
        elseif ($order_status == 'pending payment') { $order_action = false; }
        else { $order_action = false;}

        $sitenme = get_bloginfo( 'name' );
        
        // Orderdata
        $orderdata = array(
            "provider"          => "WooCommerce",
            "email"             => $email,
            "action"            => $order_action,
            "occurred_at"       => $order_data['date_created']->date_i18n('c'),
            "order_id"          => strval($order_data['id']),
            "order_public_id"   => strval($order_data['id']),
            "grand_total"       => intval($ordertotal),
            "total_discounts"   => intval($discounttotal),
            "total_taxes"       => intval($ordertaxes),
            //"total_fees"      => '0',
            "total_shipping"    => intval($totalshipping), 
            "currency"          => $order_data['currency'],
            "order_url"         => $orderurl,
            "billing_address"   => $billing,
            "shipping_address"  => $shipping,
            "items"             => $item_list
        );
        
        if ($order_action != FALSE) {
            $this->drip->drip_postv3('shopper_activity/order',$orderdata);
        }
        //echo json_encode($orderdata);
    }

}