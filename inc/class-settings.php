<?php
/**
 * Drippo: Admin Class
 *
 * This is where all of the adminy things are.
 *
 * @package drippo
 * @since 1.0.0
 */
namespace drippo;
defined( 'DRIPPO_VERSION' ) || exit;

class Settings extends Base {
    public $apikey;
    public $accountnumber;
    public $admin;
    
    public function __construct(){
        $this->register_settings();
        $this->apikey = get_option('drippo_api_key');
        $this->accountnumber = get_option('drippo_account_number');
        $this->admin = Admin::get_instance();
        add_action( 'admin_notices', array($this, 'key_check') );
    }

    /**
	 * Registers the plugin options.
	 */
    public function register_settings(){

        add_filter( 'drippo_sanitize_text', 'sanitize_text_field' );
        add_filter( 'drippo_ssanitize_password', 'sanitize_text_field' );

        add_option( 'drippo_api_key', '');
        add_option( 'drippo_account_number', '');


        // drip api connection settings
        register_setting( 'drippo_options_group', 'drippo_api_key', 'drippo_ssanitize_password' );
        register_setting( 'drippo_options_group', 'drippo_account_number', 'sanitize_text_field' );

    }

    /**
	 * Shows admin notice if key or acc number is not entered.
	 */
    public function key_check(){
        if (empty( get_option('drippo_api_key')) or empty( get_option('drippo_account_number')) ){
            $this->admin->wordpress_notice('warning','Drippo requires your Drip API key and account number - <a href="options-general.php?page=Drippo">Drippo Settings</a>');
        }
    }
    
}