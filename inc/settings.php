<?php
/***
 * Settings page fields
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="wrap" id="drippo">
    <h2>Drippo <span style='font-size:13px;'>v<?php echo DRIPPO_VERSION;?></span><br><span class="subtitle" style='padding-left:0;'>Drip for WooCommerce by <a href="https://unfamo.us">Mike Hancock</a></span></h2>
    <div class="container">
    <form method="post" action="options.php" data-parsley-validate>
    <?php settings_fields( 'drippo_options_group' ); ?> 
        <div class="panel">
            <div class="panel-header">
                <div class="panel-title">Drip Connection Settings</div>
            </div>
            <div class="panel-body">
                <div class="columns">
                    <div class="column col-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="drippo_api_key">API Token</label>
                            <input type="password" id="drippo_api_key" class="form-input" name="drippo_api_key" value="<?php echo get_option('drippo_api_key'); ?>" placeholder="API Key" required data-parsley-type="alphanum" data-parsley-error-message="Your API key is looking off, maybe try that again?"/>
                        </div>
                    </div>
                    <div class="column col-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-label" for="drippo_account_number">Account Number</label>
                            <input type="text" id="drippo_account_number" class="form-input" name="drippo_account_number" value="<?php echo get_option('drippo_account_number'); ?>" placeholder="Account Number" required data-parsley-type="integer" data-parsley-error-message="This field only accepts numbers"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <p>Find your API Token <a href="https://www.getdrip.com/user/edit">here</a>, and account number in the URL when you login to drip.</p>
            </div>
        </div>
        
    <?php submit_button(); ?>
    </form>
</div>